import pytest
import librosa


from fonction_complete import transform
from fonction_complete import compare
from fonction_complete import maxima
from fonction_complete import comparaison


def test_transform():
    assert len(transform("C.mp3")[0][1]) == 7   # Vérification du nombre de beats détéctés, cependant librosa.beat.beat_track ne prend pas en compte la dernière note.
    assert transform("C.mp3")[2] == 22050       # Vérification de la fréquence d'échantillonage qui est par défault paramétré à 22050 Hz
    assert abs(len(transform("C.mp3")[1])/transform("C.mp3")[2] - librosa.core.get_duration(librosa.load("C.mp3")[0])) == 0.0   #Vérification que le nombre d'échantillons divisé par la fréquence d'échantillonage est bien égale à la durée du fichier audio


def test_compare():
    assert compare(transform("C.mp3")[0][1],transform("C.mp3")[1],transform("C.mp3")[2], transform("C.mp3")[0][1], transform("C.mp3")[1], transform("C.mp3")[2]) == 100  #Vérifie que la note sur 100 représentant la similarité du tempo, en comparant le meme fichier audio à l'aide de la fonction "compare", est bien de 100


def test_maxima():
    assert maxima([1,3,5,3,1]) == [(2,5)] # vérifie que le maximum de cette liste est 5 et est à la deuxième position
    assert maxima([0,0,0]) == [] # vérifie que cette liste n'a pas de maximum


def test_comparaison() :
    assert comparaison('C.mp3','C.mp3') == 100 # vérifie que deux morceaux identiques donnent une ressemblance de 100 sur 100
