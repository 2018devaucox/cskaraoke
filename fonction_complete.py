import librosa
import numpy as np


def transform(song):
    """Fonction qui prend en entrée une chanson et retourne 3 paramètres :

    - un premier paramètre de taille 2 comportant le tempo moyen du fichier audio (en bpm) et la conversion du
   fichier en un tableau représentant l'emplacement des beats du morceau

    - y le tableau de décomposition temporelle du fichier audio dépendant de la fréquence d'échantillonage

    - sr la fréquence d'échantillonage (par défault sr=22050) """


    y, sr = librosa.load(song)

    return (librosa.beat.beat_track(y,sr),y,sr)


def compare (a,ya,sra,b,yb,srb):

    """Fonction qui prend en entrée pour deux morceaux :- le tableau représentant l'emplacement des beats du morceau
                                                        - la fréquence d'échantillonage
                                                        - le tableau de décomposition temporelle du fichier audio
        Retourne une note sur 100 représentant la similarité, par rapport au rythme, de ces deux chansons"""

#On découpe la chanson en petit intervalle et on sort un tableau dtempo contenant le tempo moyen sur ces petits intervalles

    onset_enva = librosa.onset.onset_strength(ya, sr=sra)
    dtempoa = librosa.beat.tempo(onset_envelope=onset_enva,sr=sra ,aggregate=None)

    onset_envb = librosa.onset.onset_strength(yb, sr=srb)
    dtempob = librosa.beat.tempo(onset_envelope=onset_envb,sr=srb ,aggregate=None)

    l=len(dtempoa)
    n=0
    liste=[]

#On mesure l'écart max de tempo pour la chanson de référence :

    for j in dtempoa:
        for k in dtempoa:

            liste.append(abs(j-k))

    x=max(liste)

# On mesure l'écart de tempo du même intervalle entre la chanson a et la chanson b, si cet écart est supérieur
# à une valeur seuil définie manuellement suite à des tests sur plusieurs chansons on compte une erreur à l'utilisateur
# ie. n augmente d'un

    for i in range(0,l):


        if abs(dtempoa[i]-dtempob[i])/x >= 0.5:

            n=n+1
        else:
            continue

# on retourne la note sur 100 :

    return(100-((n/l)*100))


def maxima(l) :

    """

    :param l: liste d'éléments comparables
    :return: une liste de 2-uplets représentant les maxima ayant chacun en premier élément la coordonnée du maximum et en second élément la valeur du maximum
    """
    liste_maxima = []
    n = len(l)
    for i in range(1,n-1) :
        if l[i-1] < l[i] and l[i+1] < l[i] :
            liste_maxima.append((i,l[i]))
    return liste_maxima

def comparaison(modele, enregistrement) :

    """
    :param modele: un fichier audio (mp3 ou wav)
    :param enregistrement: un fichier audio (mp3 ou wav)
    :return: un float entre 0 et 100 caractérisant la ressemblance fréquentielle entre le fichier audio modèle et le fichier audio enregistré, 100 caractérisant l'exacte similitude et 0 la parfaite dissemblance
    """

    fourier_modele = np.abs(librosa.core.stft(librosa.core.load(modele)[0]))
    fourier_enregistrement = np.abs(librosa.core.stft(librosa.core.load(enregistrement)[0]))

    # fourier_modele et fourier_enregistrement sont des tableaux avec en colonne des tranches de temps, en lignes des tranches de fréquence et en élément l'amplitude de cette fréquence à cet instant

    time = min(np.size(fourier_modele,1),np.size(fourier_enregistrement,1)) # time représente le temps que dure le plus court des deux fichiers audio
    frequency_scale = np.size(fourier_modele,0) # frequency_scale représente les fréquences présentes dans le tableau, c'est le même pour les deux tableaux

    score = 0 # score représente la quantité de tranches de temps durant lesquelles l'interprète a joué la bonne note

    for j in range(time) : # on balaie les colonnes, c'est-à-dire le temps

        note_modele,max_modele = maxima(list(fourier_modele[:,j]))[0]
        note_enregistrement,max_enregistrement = maxima(list(fourier_enregistrement[:,j]))[0]

        # la note est la fréquence du plus petit des maxima de fréquence, c'est-à-dire la fondamentale
        # le max est l'amplitude de la fondamentale

        if max_modele < 1 :
            if max_enregistrement < 1 :
                score += 1

                # si l'amplitude maximale est faible, cela signifie qu'il y a un silence, durant lequel on ne peut définir de note jouée, il faut donc vérifier que l'interprète fasse aussi un silence

        elif note_modele == note_enregistrement :
            score += 1

            # sinon, il faut vérifier que les deux notes ont la même fréquence, donc sont une seule et même note

    return ((score/time)*100) # il ne reste plus qu'à diviser le nombre de tranches durant lesquelles la bonne note a été jouée par le nombre total de tranches, puis de multiplier par 100 pour avoir une note entre 0 et 100



def fonction_final(chanson_a,chanson_b):

    """Fonction qui prend deux fichiers audio et qui ressort une note
    sur 100 indiquant la similarité entre ces deux fichiers :

    chansona = chanson de référence
    chansonb = chanson jouée par l'utilisateur"""

#Pour pouvoir analyser la chanson on doit la convertir en un objet lisible par librosa à savoir un tableau de nombre :

#On transforme les deux chansons en tableau :

    a,ya,sra = transform(chanson_a)

    b,yb,srb = transform(chanson_b)

    note1 = compare(a[1],ya,sra,b[1],yb,srb)
    note2=comparaison(chanson_a, chanson_b)

    return "Votre note est "+ str(round((note1+note2)/2,2))+"/100"


print(fonction_final('C.mp3','cmin.mp3'))
