import pyaudio
import wave
from pydub import AudioSegment

from fonctionfinal import fonction_final
import ffmpeg

def record():

    CHUNK = 1024
    FORMAT = pyaudio.paInt16
    CHANNELS = 2
    RATE = 44100
    RECORD_SECONDS = 15
    WAVE_OUTPUT_FILENAME = "output.wav"

    p = pyaudio.PyAudio()

    stream = p.open(format=FORMAT,channels=CHANNELS,rate=RATE,input=True,frames_per_buffer=CHUNK)
    print("* recording")

    frames = []

    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        data = stream.read(CHUNK)#peut-être un nombre d'octets par frame, ah non en fait la véritable valeur est de 4096 octets
        frames.append(data)

    print("* done recording")

    stream.stop_stream()
    stream.close()
    p.terminate()

    #frames : liste des frames audio, on va envoyer frame par frame dans le projet final

    #plus qu'à trouver le moyen de lire tout ça

    wf = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
    wf.setnchannels(CHANNELS)
    wf.setsampwidth(p.get_sample_size(FORMAT))
    wf.setframerate(RATE)
    wf.writeframes(b''.join(frames))
    wf.close()

    AudioSegment.from_wav(r'C:\Users\xdeva\PycharmProjects\cskaraoke\output.wav').export(r'C:\Users\xdeva\PycharmProjects\cskaraoke\output.mp3', format="mp3")
    print('done exporting')


