from django.conf.urls import url
import numpy
from Ecoute.models import Musique

from Ecoute import views
urlpatterns=[
    url(r'^$', views.index),          ##vue associé à /Ecoute/ est index on affiche toute les musique que l'on peut écouter
    url(r'^(?P<nom>[a-z]+)/$', views.ecoute),  ## la vue associé à /Ecoute/"nom_de_la_musique" est la vue ecoute(request,nom_de_la_musique)
    url(r'^(?P<nom>[a-z]+)/NOTE/$', views.note)     ## affiche la note en comparant avec l'original et on affiche la note
]
