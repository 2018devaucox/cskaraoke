from django.test import TestCase

# Create your tests here.
from django.urls import resolve
from django.test import TestCase
from Ecoute.views import accueil,index
from django.http import HttpResponse, HttpRequest

# Create your tests here.


class WelcomePageTest(TestCase):

    def test_root_url_resolves_to_welcome_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, accueil)

    def test_welcome_page_returns_correct_html(self):
        request = HttpRequest()
        response = accueil(request)
        html = response.content.decode('utf8')
        message = "bienvenue Sur CSkaraoke"
        self.assertTrue(html==message)

    def test_Ecoute_url_resolves_to_index(self):
        found = resolve('/Ecoute/')
        self.assertEqual(found.func, index)

    def test_Ecoute_url_resolves_to_index(self):
        found = resolve('/Ecoute/')
        self.assertEqual(found.func, index)


