Ce projet consiste en la réalisation d'une plateforme de karaoke instrumental en ligne.
Une personne joue ou chante un morceau, sa prestation est enregistrée et
analysée par le programme et une note lui est attribuée pour voir sa similarité 
avec l'enregistrement de départ. On a utilisé des gammes de Do majeur et mineur 
pour tester notre programme

Pour les divers étapes du programme, les packages suivants sont nécessaires:

- Librosa
- Pyaudio
- FFmpeg (Windows)
- Django 
- Numpy 

On a utilisé l'interpréteur python 3.6, afin de faire fonctionner pyaudio. 
Enfin, la plupart des chemins ont été fait en absolu, et il faudrait les modifier.
